## Installation von Oracle Database 18c XE und des Instantclients unter Docker

Eine Installation der Oracle Database und des Instantclients ist nicht unter jedem Betriebssystem ohne weitere Komplikationen möglich. 
Daher empfiehlt sich eine Installation unter Docker 

---

- [Vorbereitung](#vorbereitung)
- [Build Docker Container](#build-image)
- [Initialisierung des Containers](#initialisierung-des-containers)
- [Wartung](#wartung)

---

#### Vorbereitung 
###### Docker vorbereiten (***optional***):

Es ist empfehlenswert dem Docker Container ein eigenes Netzwerk zuzuordnen um eine einfache Kommunikation mit anderen Cotainern (z.B. für einen Instantclient) zu ermöglichen.

` sudo docker network create oracle_network
`

---

###### Download 
Download Oracle Database 18c XE from [official Website](https://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)


```
mkdir temp && cd temp
wget https://download.oracle.com/otn-pub/otn_software/db-express/oracle-database-xe-18c-1.0-1.x86_64.rpm 
```

---

##### Build Image 

```
git clone git@github.com:fuzziebrain/docker-oracle-xe.git
mv oracle-database-xe-18c-1.0-1.x86_64.rpm docker-oracle-xe/files/
cd docker-oracle-xe
docker build -t oracle-xe:18c .
```

---

##### Initialisierung des Containers

Es ist erforderlich mittels `mkdir` den Ordner des Volumes zu erstellen, bzw. einen Vorhandenen Ordner zuwählen.

```bash
sudo docker run -d \
   -p 32118:1521 \
   -p 35518:5500 \
   -e ORACLE_PWD=Oracle18 \
   --name=oracle-xe \
   --volume ~/vm/docker/oracle-xe:/opt/oracle/oradata \
   --network=oracle_network \
   oracle-xe:18c

# Nimmt einige Zeit in Anspruch, Status abfragen mit:
sudo docker logs oracle-xe
```

---

##### Wartung 

```
- Starten des Containers
docker start oracle-xe

- Stoppen des Containers
sudo docker stop -t 60 oracle-xe

- Shell im den Container
sudo docker exec -it oracle-express18 bash -c "source /home/oracle/.bashrc; bash"

- SQL*Plus im Container benutzen
$ORACLE_HOME/bin/sqlplus sys/Oracle18@localhost/XE as sysdba
```


